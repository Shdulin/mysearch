<?php

/**
 * @file
 * My Search page file.
 */


/**
 * Menu title callback.
 *
 * @param string $searchterm
 *   Search term.
 *
 * @return string
 *   Title string for the My Search page.
 */
function mysearch_searchtitle($searchterm) {
  return t('Search for: @searchterm', array('@searchterm' => $searchterm));
}


/**
 * Menu page callback.
 *
 * Provides a simple list of nodes matching the
 * search term Example: hitting the URL:
 *   http://domain.com/mysearch/example
 * will return a list of links to nodes which have the word
 * example in them.
 *
 * @param string $searchterm
 *   Search term.
 *
 * @return array
 *   Render array which creates the page content.
 */
function mysearch_searchpage($searchterm) {
  $query = new EntityFieldQuery();

  $query->entityCondition('entity_type', 'node')
    ->propertyCondition('status', NODE_PUBLISHED)
    ->fieldCondition('body', 'value', '%' . check_plain($searchterm) . '%', 'like')
    ->pager(20);

  $result = $query->execute();

  $items = array();
  if (isset($result['node'])) {
    $nids = array_keys($result['node']);
    $nodes = node_load_multiple($nids);
    foreach ($nodes as $node) {
      $item = l($node->title, 'node/' . $node->nid, array(
          'attributes' => array('title' => $node->title))
      );

      if (user_access('access mysearch debug information')) {
        $item .= '<!-- debugging output: ' . print_r($node, 1) . '  -->';
      }

      $items[] = $item;
    }
  }
  else {
    return t('No results found.');
  }

  return array(
    'results' => array(
      '#theme' => 'item_list',
      '#items' => $items,
    ),
    'pager' => array(
      '#theme' => 'pager',
    ),
  );
}
